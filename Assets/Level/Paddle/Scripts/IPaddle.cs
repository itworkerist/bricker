﻿using UnityEngine;

namespace Bricker.Level
{
    public enum PaddleMode
    {
        Default = 0,
        Sticky,
        Falling
    }

    public interface IPaddle
    {
        void PostConstruct(IPaddleContext context, PaddleConfig config);

        int MinLength { get; }

        int MaxLength { get; }

        float Length { get; }

        Vector3 StartPoint { get; }

        Vector3 EndPoint { get; }

        bool IsMoving { get; }

        bool IsCompleted { get; }

        PaddleMode Mode { get; set; }

        void Restart(PaddleMode mode);

        void Begin(Vector3 point);

        void Move(Vector3 point);

        void End();
    }
}
