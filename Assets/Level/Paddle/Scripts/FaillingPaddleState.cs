﻿namespace Bricker.Level
{
    public partial class Paddle
    {
        private partial class FallingPaddleState : DefaultPaddleState
        {
            public FallingPaddleState(Paddle paddle, PaddleConfig config) : base(paddle, config) => FallingSpeed = config.fallingSpeed;

            public float FallingSpeed { get; private set; }

            public override void Update(float deltaTime) {
                base.Update(deltaTime);

                if (_paddle.IsCompleted) {
                    _paddle._transform.Translate(0, 0, -FallingSpeed * deltaTime);
                }
            }
        }
    }
}
