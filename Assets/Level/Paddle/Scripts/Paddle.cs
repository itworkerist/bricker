﻿using UnityEngine;

namespace Bricker.Level
{
    [DisallowMultipleComponent]
    public partial class Paddle : MonoBehaviour, IPaddle
    {
        private partial class DefaultPaddleState {}
        private partial class FallingPaddleState {}
        private partial class StickyPaddleState {}

        [SerializeField] private Transform _transform;
        [SerializeField] private SpriteRenderer _originRenderer;
        [SerializeField] private LineRenderer _lineRenderer;
        [SerializeField] private GameObject _startGameObject;
        [SerializeField] private GameObject _endGameObject;
        [SerializeField] private GameObject _ropeGameObject;
        [SerializeField] private Animation _ropeAnimation;
        [SerializeField] private GameObject _stickyStartGameObject;
        [SerializeField] private GameObject _stickyEndGameObject;

        private IPaddleContext _context;
        private DefaultPaddleState _state;
        private DefaultPaddleState[] _states;

        public void PostConstruct(IPaddleContext context, PaddleConfig config) {
            _context = context;
            MinLength = config.minLength;
            MaxLength = config.maxLength;

            _states = new [] {
                new DefaultPaddleState(this, config),
                new FallingPaddleState(this, config),
                new StickyPaddleState(this, config)
            };
            _state = _states[(int)PaddleMode.Default];

            Restart(PaddleMode.Default);
        }

        protected void OnCollisionExit(Collision collision) {
            Debug.Assert(IsCompleted);
            if (IsCompleted) {
                _state.Collide(collision);
            }
        }

        protected void Update() => _state.Update(Time.deltaTime);

        public int MinLength { get; private set; }

        public int MaxLength { get; private set; }

        public Vector3 StartPoint { get; private set; }

        public Vector3 EndPoint { get; private set; }

        public float Length => Vector3.Distance(StartPoint, EndPoint);

        public void Restart(PaddleMode mode) {
            StartPoint = Vector3.zero;
            EndPoint = StartPoint;
            _transform.position = Vector3.zero;

            _originRenderer.enabled = false;
            _lineRenderer.enabled = false;

            _startGameObject.SetActive(false);
            _endGameObject.SetActive(false);
            _ropeGameObject.SetActive(false);

            _ropeAnimation.Stop();
            _ropeAnimation.Rewind();

            Mode = mode;
        }

        public void Begin(Vector3 point) {
            Debug.Assert(!IsMoving);

            Debug.Assert(point.y >= 0);
            StartPoint = point;
            EndPoint = StartPoint;

            _originRenderer.enabled = true;
            _originRenderer.transform.position = StartPoint;

            _lineRenderer.enabled = true;
            _lineRenderer.SetPosition(0, StartPoint);
            _lineRenderer.SetPosition(1, EndPoint);

            _startGameObject.SetActive(false);
            _startGameObject.transform.position = new Vector3(StartPoint.x, _startGameObject.transform.position.y, StartPoint.z);
            _endGameObject.SetActive(false);
            _ropeGameObject.SetActive(false);
        }

        public void Move(Vector3 point) {
            Debug.Assert(IsMoving);
            if (!IsMoving) {
                return;
            }

            EndPoint = point;
            Debug.Assert(Mathf.Approximately(StartPoint.y, EndPoint.y));

            if (Length < MinLength) {
                EndPoint = StartPoint + (EndPoint - StartPoint).normalized * MinLength;
            }

            if (Length > MaxLength) {
                EndPoint = StartPoint + (EndPoint - StartPoint).normalized * MaxLength;
            }

            _lineRenderer.SetPosition(1, EndPoint);
            _lineRenderer.sharedMaterial.mainTextureScale = new Vector2(Length, 1);
            _endGameObject.transform.position = new Vector3(EndPoint.x, _endGameObject.transform.position.y, EndPoint.z);
            _state.Reshape();
        }

        public void End() {
            Debug.Assert(IsMoving);
            if (!IsMoving) {
                return;
            }

            if (Length < MinLength) {
                Move(StartPoint + Vector3.right * MinLength); // Shift from an origin point to complete
            }

            _originRenderer.enabled = false;
            _lineRenderer.enabled = false;

            _startGameObject.SetActive(true);
            _endGameObject.SetActive(true);

            Vector3 center = (StartPoint + EndPoint) / 2f;
            center.y = _ropeGameObject.transform.position.y;
            _ropeGameObject.transform.position = center;
            _ropeGameObject.transform.eulerAngles = new Vector3(0, Mathf.Atan2(EndPoint.z - StartPoint.z, EndPoint.x - StartPoint.x) * -Mathf.Rad2Deg, 0);
            
            Vector3 scale = _ropeGameObject.transform.localScale;
            scale.x = Length;
            _ropeGameObject.transform.localScale = scale;
            _ropeGameObject.SetActive(true);

            _ropeAnimation.Stop();
            _ropeAnimation.Rewind();
            
            Debug.Assert(IsCompleted);
        }

        public bool IsMoving => _originRenderer.enabled;

        public bool IsCompleted => !IsMoving && StartPoint != EndPoint;

        public PaddleMode Mode {
            get => _mode;
            set {
                _state.Leave();
                _mode = value;
                _state = _states[(int)value];
                _state.Enter();
            }
        }
        private PaddleMode _mode;
    }
}
