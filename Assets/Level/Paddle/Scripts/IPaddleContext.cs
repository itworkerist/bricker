﻿namespace Bricker.Level
{
    public interface IPaddleContext
    {
        IBallAimer BallAimer { get; }

        IBall SelectedBall { get; }
    }
}
