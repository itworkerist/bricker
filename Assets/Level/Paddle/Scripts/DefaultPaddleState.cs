﻿using UnityEngine;

namespace Bricker.Level
{
    public partial class Paddle
    {
        private partial class DefaultPaddleState
        {
            protected readonly Paddle _paddle;

            public DefaultPaddleState(Paddle paddle, PaddleConfig config) {
                _paddle = paddle;
                BallSpeedMultiplier = config.ballSpeedMultiplier;
            }

            public float BallSpeedMultiplier { get; private set; }

            public virtual void Enter() => Reshape();

            public virtual void Leave() => Reshape();

            public virtual void Collide(Collision collision) {
                GameObject go = collision.gameObject;
                if (go.CompareTag(LevelConsts.kBallTag)) {
                    IBall ball = go.GetComponent<IBall>();
                    ball.SpeedPaddleSummand = _paddle.Length * BallSpeedMultiplier;
                    ball.SpeedBrickMultiplier = 1; // Disable acceleration caused by bomb
                    _paddle._ropeAnimation.Play();
                }
            }

            public virtual void Update(float deltaTime) {}

            public virtual void Reshape() {}
        }
    }
}
