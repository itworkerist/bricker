﻿using UnityEngine;

namespace Bricker.Level
{
    public partial class Paddle
    {
        private partial class StickyPaddleState : DefaultPaddleState
        {
            private IBall _ball;

            public StickyPaddleState(Paddle paddle, PaddleConfig config) : base(paddle, config) {
                _ball = null;
                _paddle._stickyStartGameObject.GetComponent<LighteningScript>().Initialize();
            }

            public override void Enter() {
                base.Enter();
                _paddle._stickyStartGameObject.SetActive(true);
                _paddle._stickyEndGameObject.SetActive(true);
            }

            public override void Leave() {
                base.Leave();
                _paddle._transform.position = Vector3.zero;
                _paddle._stickyStartGameObject.SetActive(false);
                _paddle._stickyEndGameObject.SetActive(false);
            }

            public override void Collide(Collision collision) {
                GameObject go = collision.gameObject;
                if (go.CompareTag(LevelConsts.kBallTag)) {
                    _paddle._ropeAnimation.Stop();
                    _paddle._ropeAnimation.Rewind();

                    Debug.Assert(_ball == null);
                    _ball = go.GetComponent<IBall>();
                    Debug.Assert(_ball == _paddle._context.SelectedBall && !_ball.Sleeping);
                    _ball.Skilled = false;
                    _ball.Sleeping = true;

                    Debug.Assert(!_paddle._context.BallAimer.Active);
                    _paddle._context.BallAimer.Active = true;

                    Reshape();
                    return;
                }

                base.Collide(collision);
            }

            public override void Update(float deltaTime) {
                base.Update(deltaTime);
                Reshape();
            }

            public override void Reshape() {
                if (_ball == null) {
                    return;
                }

                Debug.Assert(_ball == _paddle._context.SelectedBall);
                if (_ball != _paddle._context.SelectedBall) {
                    _ball = _paddle._context.SelectedBall;

                    Debug.Assert(_ball != null);
                    if (_ball == null) {
                        _paddle._context.BallAimer.Active = false;
                        return;
                    }
                }

                Debug.Assert(_paddle.IsMoving || _paddle.IsCompleted);
                if (!_ball.Sleeping || (!_paddle.IsMoving && !_paddle.IsCompleted)) {
                    _paddle._context.BallAimer.Active = false;
                    _ball = null;
                    return;
                }

                Vector3 start = (_paddle.StartPoint.x < _paddle.EndPoint.x) ? _paddle.StartPoint : _paddle.EndPoint;
                Vector3 end = (_paddle.StartPoint.x < _paddle.EndPoint.x) ? _paddle.EndPoint : _paddle.StartPoint;

                Vector3 center = (start + end) / 2f;
                Vector3 perpendicular = end - start;
                perpendicular.Set(-perpendicular.z, 0f, perpendicular.x);
                perpendicular.Normalize();

                float ballOffset = _ball.Radius * 3f;
                Vector3 ballLocation = center + perpendicular * ballOffset;
                _ball.Locate(ballLocation);
                _paddle._context.BallAimer.Locate(ballLocation);
                
                if (perpendicular.sqrMagnitude > 0) {
                    _ball.Direct(perpendicular);
                    _paddle._context.BallAimer.Aim(ballLocation + perpendicular * ballOffset);
                }
            }
        }
    }
}
