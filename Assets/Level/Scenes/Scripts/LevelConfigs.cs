﻿using System;

namespace Bricker.Level
{
    [Serializable]
    public class LevelConfig
    {
        public PlaygroundConfig playground;
        public ScoreConfig score;
    }

    [Serializable]
    public class PlaygroundConfig
    {
        public float ballSkillLongTapMinDuration;
        public CapsulesConfig capsules;
        public PaddleConfig paddle;
        public BallConfig simpleBall;
        public SkillfulBallConfig iceBall;
        public MagneticBallConfig magneticBall;
        public BrickConfig simpletonBrick;
        public BombBrickConfig bombBrick;
        public CannonBrickConfig cannonBrick;
    }

    [Serializable]
    public class PaddleConfig
    {
        public int minLength;
        public int maxLength;
        public float fallingSpeed;
        public float ballSpeedMultiplier;
    }

    [Serializable]
    public class BallConfig
    {
        public float speed;
        public int damage;
    }

    [Serializable]
    public class SkillfulBallConfig : BallConfig
    {
        public float skillDuration;
        public float skillCooldown;
    }

    [Serializable]
    public class MagneticBallConfig : SkillfulBallConfig
    {
        public float skillTimeInterval;
        public float distanceBallSpeedReduction;
    }

    [Serializable]
    public class BrickConfig
    {
        public int health;
        public float freezeDuration;
        public int capsuleChance;
    }

    [Serializable]
    public class BombBrickConfig : BrickConfig
    {
        public float ballSpeedMultiplier;
    }

    [Serializable]
    public class CannonBrickConfig : BrickConfig
    {
        public float impactMultiplier;
        public float impactTolerance;
    }

    [Serializable]
    public class CapsulesConfig
    {
        public float maxChance;
        public float minSpeed;
        public float maxSpeed;
        public float bigBallDamageMultiplier;
        public float bigBallRadiusMultiplier;
        public float smallBallRadiusMultiplier;
        public float smallBallDamageMultiplier;
        public float slowBallSpeedMultiplier;
        public float quickBallSpeedMultiplier;
    }

    [Serializable]
    public class ScoreConfig
    {
        public int winMultiplier;
        public int brickDeathSummand;
        public int capsuleAppliedSummand;
    }
}
