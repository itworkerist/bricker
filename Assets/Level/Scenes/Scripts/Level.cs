﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Bricker.Level
{
    [DisallowMultipleComponent]
    public class Level : MonoBehaviour, IPlaygroundHandler
    {
        [SerializeField] private LevelUI _ui;
        [SerializeField] private Playground _playground;
        [SerializeField] private BallAimer _ballAimer;
        [SerializeField] private Paddle _paddle;
        [SerializeField] private Component _bricksRoot;
        [SerializeField] private BallCategory[] _inventoryBalls;
        [SerializeField] private LevelConfig _config;

        private float _progress;

        protected void Start() {
            _progress = 1;
            Time.timeScale = 1;
            Score = 0;

            var factory = new Factory<BallCategory>("{0}Ball");
            var balls = new List<IBall>(_inventoryBalls.Length);
            balls.AddRange(_inventoryBalls.Select(category => factory.Create<IBall>(category, _playground.transform)));
            _playground.PostConstruct(this, _ballAimer, _paddle, _config.playground, balls, _bricksRoot.GetComponentsInChildren<IBrick>());
        }

        private int Score {
            get => _score;
            set {
                _score = value;
                _ui.SetScore(Score);
            }
        }
        private int _score;

        public void OnInventoryChanged(IReadOnlyCollection<IBall> balls) {
            _ui.SetInventoryBalls(balls);
            _progress = balls.Count * 0.34f;

            if (balls.Count <= 0) {
                Lose();
            }
        }

        public void OnBallSkillPowerChanged(float skillPower) => _ui.SetBallSkillPower(skillPower);

        public void OnBrickDied(BrickCategory category, int bricksCount) {
            if (bricksCount >= 0 && category != BrickCategory.Bomb) {
                Score += _config.score.brickDeathSummand;
            }

            if (bricksCount <= 0) {
                Win();
            }
        }

        public void OnCapsuleApplied(CapsuleCategory category) {
            switch (category) {
            case CapsuleCategory.BigBall:
            case CapsuleCategory.SlowBall:
            case CapsuleCategory.StickyPaddle:
                Score += _config.score.capsuleAppliedSummand;
                break;
            case CapsuleCategory.SmallBall:
            case CapsuleCategory.QuickBall:
            case CapsuleCategory.FallingPaddle:
                Score -= (_config.score.capsuleAppliedSummand <= Score) ? _config.score.capsuleAppliedSummand : Score;
                break;
            default:
                Debug.LogErrorFormat("Score by capsule category = {0} not implemented!", category);
                return;
            }
        }

        private void Win() {
            Score *= _config.score.winMultiplier;
            Time.timeScale = 0;
            _playground.enabled = false;
            _ui.ShowWinLosePopup(_progress);
        }

        private void Lose() {
            _progress = 0;
            Time.timeScale = 0;
            _playground.enabled = false;
            _ui.ShowWinLosePopup(_progress);
        }
    }
}
