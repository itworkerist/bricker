﻿namespace Bricker.Level
{
    public static class LevelConsts
    {
        public const string kWallTag = "Wall";
        public const string kWallBottomName = "Bottom";
        public const string kPaddleTag = "Paddle";
        public const string kBallTag = "Ball";
        public const string kBrickTag = "Brick";
        public const string kCapsuleTag = "Capsule";
    }
}
