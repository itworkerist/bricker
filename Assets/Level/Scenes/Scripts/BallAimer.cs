﻿using UnityEngine;

namespace Bricker.Level
{
    public interface IBallAimer
    {
        bool Active { get; set; }

        Vector3 Origin { get; }

        Vector3 Direction { get; }

        void Aim(Vector3 point);

        void Locate(Vector3 location);
    }

    [DisallowMultipleComponent]
    public class BallAimer : MonoBehaviour, IBallAimer
    {
        [SerializeField] private int _lineWidth;
        [SerializeField] private int _tailLength;
        [SerializeField] private float _animationSpeed;
        [SerializeField] private LineRenderer _bodyLineRenderer;
        [SerializeField] private LineRenderer _tailLineRenderer;
        
        private Vector2 _lineTextureOffset;
        private Vector3 _startPoint;
        private Vector3 _middlePoint;
        private Vector3 _endPoint;
        private bool _hasChanged;

        private static RaycastHit[] _sRaycastHits;

        protected void Awake() {
            _bodyLineRenderer.positionCount = 2;
            _bodyLineRenderer.startWidth = _lineWidth;
            _bodyLineRenderer.endWidth = _lineWidth;

            _tailLineRenderer.positionCount = 2;
            _tailLineRenderer.startWidth = _lineWidth;
            _tailLineRenderer.endWidth = _lineWidth;

            Debug.Assert(_bodyLineRenderer.sharedMaterial.mainTextureOffset == _tailLineRenderer.sharedMaterial.mainTextureOffset);
            _lineTextureOffset = _bodyLineRenderer.sharedMaterial.mainTextureOffset;

            _startPoint = transform.position;
            _middlePoint = _startPoint;
            _endPoint = _startPoint;
            _hasChanged = true;

            if (_sRaycastHits == null) {
                const int kMaxNumberOfRaycastHits = 64;
                _sRaycastHits = new RaycastHit[kMaxNumberOfRaycastHits];
            }
        }

        protected void Update() {
            _lineTextureOffset.x -= Time.deltaTime * _animationSpeed;
            _bodyLineRenderer.sharedMaterial.mainTextureOffset = _lineTextureOffset;
            _tailLineRenderer.sharedMaterial.mainTextureOffset = _lineTextureOffset;
        }

        protected void LateUpdate() => Reshape();

        private void Reshape() {
            if (!_hasChanged) {
                return;
            }

            int hitIndex = -1;

            float hitDistance = float.MaxValue;
            for (int index = 0, numHits = Physics.RaycastNonAlloc(_startPoint, Direction, _sRaycastHits); index < numHits; ++index) {
                Collider otherCollider = _sRaycastHits[index].collider;
                if (otherCollider.CompareTag(LevelConsts.kWallTag) || (otherCollider.CompareTag(LevelConsts.kBrickTag) && !otherCollider.isTrigger)) {
                    float distance = Vector3.Distance(_startPoint, _sRaycastHits[index].point);
                    if (distance < hitDistance) {
                        hitIndex = index;
                        hitDistance = distance;
                    }
                }
            }

            if (hitIndex < 0) {
                return;
            }

            RaycastHit hit = _sRaycastHits[hitIndex];
            _middlePoint = hit.point;
            Vector3 reflection = Vector3.Reflect(_middlePoint - _startPoint, hit.normal);
            reflection.y = 0;
            _endPoint = reflection.normalized * _tailLength + _middlePoint;
            _endPoint.y = _startPoint.y;

            _bodyLineRenderer.SetPosition(0, _startPoint);
            _bodyLineRenderer.SetPosition(1, _middlePoint);
            _bodyLineRenderer.material.mainTextureScale = new Vector2(Vector3.Distance(_startPoint, _middlePoint) / _lineWidth, 1);

            _tailLineRenderer.SetPosition(0, _middlePoint);
            _tailLineRenderer.SetPosition(1, _endPoint);
            _tailLineRenderer.material.mainTextureScale = new Vector2(Vector3.Distance(_middlePoint, _endPoint) / _lineWidth, 1);

            _hasChanged = false;
        }

        public bool Active {
            get => gameObject.activeSelf;
            set {
                gameObject.SetActive(value);
                _hasChanged = true;
            }
        }

        public Vector3 Origin => _startPoint;

        public Vector3 Direction => _middlePoint - _startPoint;

        public void Aim(Vector3 point) {
            _middlePoint.Set(point.x, _startPoint.y, point.z);
            _endPoint = _middlePoint;
            _hasChanged = true;
        }

        public void Locate(Vector3 position) {
            Vector3 direction = Direction;
            _startPoint.Set(position.x, _startPoint.y, position.z);
            Aim(_startPoint + direction);
        }
    }
}
