﻿using System;
using UnityEngine;

namespace Bricker.Level
{
    [DisallowMultipleComponent]
    public class InputTouch : MonoBehaviour
    {
        private int _touchFingerId;
        private TouchPhase _touchPhase;
        private Vector3 _touchPoint;

        public event Action<Vector3> TouchDown;
        public event Action<Vector3> TouchMove;
        public event Action TouchUp;

        protected void Awake() {
            Input.multiTouchEnabled = false;
            
            _touchFingerId = -1;
            _touchPhase = TouchPhase.Canceled;
            _touchPoint = Vector3.zero;
        }

        protected void Update() {
            if (Input.touchSupported) {
                if (Input.touchCount <= 0) {
                    return;
                }
                
                Touch touch = Input.GetTouch(0);
                HandleTouch(touch.fingerId, touch.phase, touch.position.x, touch.position.y);
            }
            else {
                if (Input.GetMouseButtonDown(0)) {
                    HandleTouch(0, TouchPhase.Began, Input.mousePosition.x, Input.mousePosition.y);
                }
                else if (Input.GetMouseButton(0)) {
                    HandleTouch(0, TouchPhase.Moved, Input.mousePosition.x, Input.mousePosition.y);
                }
                else if (Input.GetMouseButtonUp(0)) {
                    HandleTouch(0, TouchPhase.Ended, Input.mousePosition.x, Input.mousePosition.y);
                }
            }
        }

        private void HandleTouch(int touchFingerId, TouchPhase touchPhase, float touchPositionX, float touchPositionY) {
            switch (touchPhase) {
            case TouchPhase.Began:
                Debug.Assert(_touchPhase == TouchPhase.Canceled || _touchPhase == TouchPhase.Ended);
                _touchFingerId = touchFingerId;
                _touchPhase = TouchPhase.Began;
                _touchPoint.Set(touchPositionX, touchPositionY, 0);
                TouchDown?.Invoke(_touchPoint);
                break;
            case TouchPhase.Stationary:
            case TouchPhase.Moved:
                Debug.Assert(_touchFingerId == touchFingerId);
                Debug.Assert(_touchPhase == TouchPhase.Began || _touchPhase == TouchPhase.Stationary || _touchPhase == TouchPhase.Moved);
                if (_touchFingerId == touchFingerId) {
                    _touchPhase = touchPhase;
                    _touchPoint.Set(touchPositionX, touchPositionY, 0);
                    TouchMove?.Invoke(_touchPoint);
                }
                break;
            case TouchPhase.Canceled:
            case TouchPhase.Ended:
                Debug.Assert(_touchFingerId == touchFingerId);
                Debug.Assert(_touchPhase == TouchPhase.Began || _touchPhase == TouchPhase.Stationary || _touchPhase == TouchPhase.Moved);
                if (_touchFingerId == touchFingerId) {
                    _touchPhase = touchPhase;
                    _touchPoint.Set(touchPositionX, touchPositionY, 0);
                    TouchUp?.Invoke();
                }

                _touchFingerId = -1;
                break;
            default:
                Debug.LogError(touchPhase);
                break;
            }
        }
    }
}
