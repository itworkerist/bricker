﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
using Random = UnityEngine.Random;

namespace Bricker.Level
{
    public interface IPlaygroundHandler
    {
        void OnInventoryChanged(IReadOnlyCollection<IBall> balls);

        void OnBallSkillPowerChanged(float skillPower);

        void OnBrickDied(BrickCategory category, int bricksCount);

        void OnCapsuleApplied(CapsuleCategory category);
    }

    [DisallowMultipleComponent]
    public class Playground : MonoBehaviour, IPaddleContext, IBallContext, IBrickContext, ICapsuleHandler
    {
        [SerializeField] private BoxCollider _leftWallCollider;
        [SerializeField] private BoxCollider _rightWallCollider;
        [SerializeField] private BoxCollider _floorCollider;
        [SerializeField] private BoxCollider _floorPaddleCollider;
        [SerializeField] private CapsulePool _capsulePool;
        [SerializeField] private InputTouch _inputTouch;
        
        private IPlaygroundHandler _handler;
        private PlaygroundConfig _config;
        private Repository<IBall, BallConfig> _ballConfigs;
        private Repository<IBrick, BrickConfig> _brickConfigs;
        private Stack<IBall> _balls;
        private List<IBrick> _bricks;
        private Ground _ground;

        public void PostConstruct(IPlaygroundHandler handler, IBallAimer ballAimer, IPaddle paddle, PlaygroundConfig config, IEnumerable<IBall> balls, IEnumerable<IBrick> bricks) {
            _handler = handler;
            _config = config;
            _capsulePool.PostConstruct(this);
            _ground = new Ground(this, _floorCollider.bounds, _floorPaddleCollider.bounds, _leftWallCollider.bounds.max.x, _rightWallCollider.bounds.min.x);
            
            _ballConfigs = new Repository<IBall, BallConfig>();
            _ballConfigs.Register<SimpleBall>(_config.simpleBall);
            _ballConfigs.Register<IceBall>(_config.iceBall);
            _ballConfigs.Register<MagneticBall>(_config.magneticBall);

            _brickConfigs = new Repository<IBrick, BrickConfig>();
            _brickConfigs.Register<SimpletonBrick>(_config.simpletonBrick);
            _brickConfigs.Register<BombBrick>(_config.bombBrick);
            _brickConfigs.Register<CannonBrick>(_config.cannonBrick);

            BallAimer = ballAimer;
            BallAimer.Active = false;
            BallSpawnPosition = BallAimer.Origin;
            
            Paddle = paddle;
            Paddle.PostConstruct(this, _config.paddle);
            
            _balls = new Stack<IBall>(balls.Reverse());
            foreach (var ball in _balls) {
                ball.PostConstruct(this);
            }
            
            _bricks = new List<IBrick>(bricks);
            foreach (var brick in _bricks) {
                brick.PostConstruct(this);
            }
            
            SelectAndAimBall();
            _handler.OnInventoryChanged(_balls);
        }

        public float BallSkillLongTapMinDuration => _config.ballSkillLongTapMinDuration;

        public Vector3 BallSpawnPosition { get; private set; }

        public IBallAimer BallAimer { get; private set; }

        public IPaddle Paddle { get; private set; }
        
        public IBall SelectedBall { get; private set; }

        public bool HasSelectedBall => SelectedBall != null;

        private CapsulesConfig CapsulesConfig => _config.capsules;

        protected void OnEnable() {
            _inputTouch.TouchDown += TouchDown;
            _inputTouch.TouchMove += TouchMove;
            _inputTouch.TouchUp += TouchUp;
        }

        protected void OnDisable() {
            _inputTouch.TouchDown -= TouchDown;
            _inputTouch.TouchMove -= TouchMove;
            _inputTouch.TouchUp -= TouchUp;
        }

        private void TouchDown(Vector3 touchPoint) => _ground.TouchDown(touchPoint);

        private void TouchMove(Vector3 touchPoint) => _ground.TouchMove(touchPoint);

        private void TouchUp() => _ground.TouchUp();

        public TBallConfig GetConfig<TBallConfig>(IBall ball) where TBallConfig : BallConfig => _ballConfigs.Get<TBallConfig>(ball);

        public void OnBallSkillPowerChanged(float skillPower) => _handler.OnBallSkillPowerChanged(skillPower);

        public void OnBallDied(IBall ball) {
            Debug.Assert(HasSelectedBall && SelectedBall == ball);
            TouchUp();

            BallAimer.Active = false;
            Paddle.Restart(PaddleMode.Default);
            SelectedBall = null;

            Debug.Assert(_balls.Count > 0 && _balls.Peek() == ball);
            _balls.Pop();

            if (_balls.Count > 0 && _bricks.Count > 0) {
                SelectAndAimBall();
            }

            _handler.OnInventoryChanged(_balls);
        }

        private void SelectAndAimBall() {
            TouchUp();

            Debug.Assert(!BallAimer.Active);
            BallAimer.Active = true;
            BallAimer.Locate(BallSpawnPosition);
            BallAimer.Aim(Vector3.forward);

            Debug.Assert(!HasSelectedBall);
            SelectedBall = _balls.Peek();
            SelectedBall.Restart();
            SelectedBall.Locate(BallAimer.Origin);
            SelectedBall.Direct(BallAimer.Direction);
        }

        public TBrickConfig GetConfig<TBrickConfig>(IBrick brick) where TBrickConfig : BrickConfig => _brickConfigs.Get<TBrickConfig>(brick);

        public void OnBrickDied(IBrick brick) {
            _bricks.Remove(brick);

            if (_bricks.Count > 0 && !BallAimer.Active) {
                var values = Enum.GetValues(typeof(CapsuleCategory));
                SpawnCapsule((CapsuleCategory)values.GetValue(Random.Range(0, values.Length)), brick.Position, brick.CapsuleChance);
            }

            _handler.OnBrickDied(brick.Category, _bricks.Count);
        }

        public void OnCapsuleFinished(ICapsule capsule, bool wasCaught) {
            _capsulePool.Despawn(capsule);

            if (wasCaught && !BallAimer.Active) {
                ApplyCapsule(capsule.Category);
            }
        }

        private void ApplyCapsule(CapsuleCategory category) {
            switch (category) {
            case CapsuleCategory.StickyPaddle:
                ModifyPaddle(PaddleMode.Sticky);
                break;
            case CapsuleCategory.FallingPaddle:
                ModifyPaddle(PaddleMode.Falling);
                break;
            case CapsuleCategory.BigBall:
                ModifyBall(1, CapsulesConfig.bigBallRadiusMultiplier, CapsulesConfig.bigBallDamageMultiplier);
                break;
            case CapsuleCategory.SmallBall:
                ModifyBall(1, CapsulesConfig.smallBallRadiusMultiplier, CapsulesConfig.smallBallDamageMultiplier);
                break;
            case CapsuleCategory.SlowBall:
                ModifyBall(CapsulesConfig.slowBallSpeedMultiplier, 1, 1);
                break;
            case CapsuleCategory.QuickBall:
                ModifyBall(CapsulesConfig.quickBallSpeedMultiplier, 1, 1);
                break;
            default:
                Debug.LogErrorFormat("Applying capsule of category = {0} not implemented!", category);
                return;
            }

            _handler.OnCapsuleApplied(category);
        }

        private void ModifyPaddle(PaddleMode mode) {
            Debug.Assert(Paddle.IsCompleted);
            if (!Paddle.IsCompleted) {
                return;
            }

            Debug.Assert(Paddle.Mode != PaddleMode.Sticky || !SelectedBall.Sleeping);
            if (Paddle.Mode == PaddleMode.Sticky && HasSelectedBall && SelectedBall.Sleeping) {
                return;
            }

            if (Paddle.Mode != mode) {
                Paddle.Mode = mode;
            }
        }

        private void ModifyBall(float speedMultiplier, float radiusMultiplier, float damageMultiplier) {
            Debug.Assert(HasSelectedBall);
            if (!HasSelectedBall) {
                return;
            }

            SelectedBall.SpeedCapsuleMultiplier = speedMultiplier;
            SelectedBall.RadiusCapsuleMultiplier = radiusMultiplier;
            SelectedBall.DamageCapsuleMultiplier = damageMultiplier;
        }

        private void SpawnCapsule(CapsuleCategory category, Vector3 position, int chance) {
            if (Paddle.Mode == PaddleMode.Sticky && (Paddle.IsMoving || Paddle.IsCompleted) && HasSelectedBall && SelectedBall.Sleeping) {
                return;
            }

            if (Random.Range(0, CapsulesConfig.maxChance) <= chance) {
                _capsulePool.Spawn(category, position, Random.Range(CapsulesConfig.minSpeed, CapsulesConfig.maxSpeed));
            }
        }
    }
}
