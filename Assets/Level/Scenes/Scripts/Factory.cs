﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Bricker.Level
{
    public class Factory<TEnum> where TEnum : struct, IComparable, IConvertible, IFormattable
    {
        private readonly IDictionary<TEnum, GameObject> _prototypes = new Dictionary<TEnum, GameObject>();

        public Factory(string pathFormat) {
            foreach (TEnum key in Enum.GetValues(typeof(TEnum))) {
                _prototypes.Add(key, Resources.Load<GameObject>(string.Format(pathFormat, key)));
            }
        }

        public T Create<T>(TEnum category, Transform parent = null) => Object.Instantiate(_prototypes[category], parent).GetComponent<T>();
    }
}
