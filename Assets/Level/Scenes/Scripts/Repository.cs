﻿using System;
using System.Collections.Generic;

namespace Bricker.Level
{
    public class Repository<TKeyBase, TValueBase> where TKeyBase : class where TValueBase : class, new()
    {
        private readonly IDictionary<Type, TValueBase> _repository = new Dictionary<Type, TValueBase>();

        public void Register<TKey>(TValueBase val) where TKey : TKeyBase, new() => _repository.Add(typeof(TKey), val);

        public TValue Get<TValue>(TKeyBase obj) where TValue : TValueBase => (TValue)_repository[obj.GetType()];
    }
}
