﻿using System.Collections.Generic;
using UnityEngine;

namespace Bricker.Level
{
    [DisallowMultipleComponent]
    public class CapsulePool : MonoBehaviour
    {
        private ICapsuleHandler _handler;
        private Factory<CapsuleCategory> _factory;
        private readonly IDictionary<int, Stack<ICapsule>> _pools = new Dictionary<int, Stack<ICapsule>>();

        protected void Awake() => _factory = new Factory<CapsuleCategory>("{0}Capsule");

        public void PostConstruct(ICapsuleHandler handler) => _handler = handler;

        public ICapsule Spawn(CapsuleCategory category, Vector3 position, float speed) {
            if (!_pools.TryGetValue((int)category, out Stack<ICapsule> pool)) {
                pool = new Stack<ICapsule>();
                _pools.Add((int)category, pool);
            }

            ICapsule capsule;
            if (pool.Count > 0) {
                capsule = pool.Pop();
            }
            else {
                capsule = _factory.Create<ICapsule>(category, transform);
                capsule.PostConstruct(_handler);
            }

            capsule.Spawn(position, speed);
            return capsule;
        }

        public void Despawn(ICapsule capsule) {
            capsule.Despawn();
            _pools[(int)capsule.Category].Push(capsule);
        }
    }
}
