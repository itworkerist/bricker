﻿using UnityEngine;

namespace Bricker.Level
{
    public class Ground
    {
        private readonly Playground _playground;
        private readonly Camera _camera;

        private readonly Bounds _floorBounds;
        private readonly Bounds _floorPaddleBounds;
        private readonly float _floorPaddleMinX;
        private readonly float _floorPaddleMaxX;
        private readonly float _floorPaddleMaxZ;
        private bool _floorHasTouch;
        private float _floorTouchStartTime;
        private Vector3 _floorTouchStartPoint;
        private bool _floorLongTapIsWait;

        public Ground(Playground playground, Bounds floorBounds, Bounds floorPaddleBounds, float floorPaddleMinX, float floorPaddleMaxX) {
            _playground = playground;
            _camera = Camera.main;
            
            _floorHasTouch = false;
            _floorLongTapIsWait = false;
            _floorBounds = floorBounds;
            _floorPaddleBounds = floorPaddleBounds;
            _floorPaddleMinX = floorPaddleMinX;
            _floorPaddleMaxX = floorPaddleMaxX;
            _floorPaddleMaxZ = _floorPaddleBounds.max.z;
        }

        private IBallAimer BallAimer => _playground.BallAimer;

        private IPaddle Paddle => _playground.Paddle;

        private IBall SelectedBall => _playground.SelectedBall;

        private bool HasSelectedBall => _playground.HasSelectedBall;

        public void TouchDown(Vector3 touchPoint) {
            Debug.Assert(!_floorHasTouch);
            if (_floorHasTouch) {
                TouchUp();
            }

            Debug.Assert(!Paddle.IsMoving);
            Debug.Assert(HasSelectedBall && !SelectedBall.Skilled);
            _floorHasTouch = HasSelectedBall && RaycastBounds(touchPoint, _floorBounds);
            if (_floorHasTouch) {
                _floorTouchStartTime = Time.unscaledTime;
                _floorTouchStartPoint = touchPoint;
                _floorLongTapIsWait = !BallAimer.Active && !Paddle.IsMoving && SelectedBall.Category != BallCategory.Simple && !SelectedBall.Skilled && !SelectedBall.Sleeping;
            }
            else {
                _floorLongTapIsWait = false;
            }
        }

        public void TouchMove(Vector3 touchPoint) {
            if (!_floorHasTouch) {
                return;
            }

            Debug.Assert(HasSelectedBall);
            if (!HasSelectedBall) {
                TouchUp();
                return;
            }

            if (!RaycastBounds(touchPoint, _floorBounds, out Vector3 floorHitPoint)) {
                _floorLongTapIsWait = false;
                return;
            }

            if (TouchMoveLongTap() || TouchMovePaddle() || TouchMoveBallAimer()) {
                return;
            }
            
            TouchMoveSelectedBall(); 
            
            bool TouchMoveLongTap() {
                // Long tap for activating skill of Selected Ball
                Debug.Assert(!_floorLongTapIsWait || (!BallAimer.Active && !Paddle.IsMoving && !SelectedBall.Sleeping && !SelectedBall.Skilled));
                _floorLongTapIsWait = _floorLongTapIsWait && !BallAimer.Active && !Paddle.IsMoving && !SelectedBall.Sleeping && !SelectedBall.Skilled;
                if (_floorLongTapIsWait) {
                    if (RaycastBounds(_floorTouchStartPoint, _floorPaddleBounds, out Vector3 floorPaddleStartHitPoint)) {
                        _floorLongTapIsWait = Vector3.Distance(floorPaddleStartHitPoint, floorHitPoint) <= Paddle.MinLength;
                    }

                    if (_floorLongTapIsWait && (Time.unscaledTime - _floorTouchStartTime) >= _playground.BallSkillLongTapMinDuration) {
                        _floorLongTapIsWait = false;
                        SelectedBall.Skilled = true;
                        return true;
                    }
                }

                return _floorLongTapIsWait;
            }

            bool TouchMovePaddle() {
                if (Paddle.IsCompleted && RaycastBounds(touchPoint, _floorPaddleBounds)) {
                    if (Paddle.Mode == PaddleMode.Sticky && SelectedBall.Sleeping) {
                        Paddle.Begin(Paddle.StartPoint);
                    }
                    else if (!SelectedBall.Skilled && !SelectedBall.Sleeping) {
                        Paddle.Restart(Paddle.Mode);
                    }
                }

                if (Paddle.IsCompleted) {
                    return false;
                }
                
                if (!Paddle.IsMoving && !SelectedBall.Sleeping && !SelectedBall.Skilled) {
                    if (RaycastBounds(_floorTouchStartPoint, _floorPaddleBounds, out Vector3 floorPaddleHitPoint) || RaycastBounds(touchPoint, _floorPaddleBounds, out floorPaddleHitPoint)) {
                        floorPaddleHitPoint.x = Mathf.Clamp(floorPaddleHitPoint.x, _floorPaddleMinX, _floorPaddleMaxX); // Trim paddle to min or max x of paddle's floor
                        floorPaddleHitPoint.y = 0;
                        Paddle.Begin(floorPaddleHitPoint);
                    }
                }

                if (!Paddle.IsMoving) {
                    return false;
                }
                
                if (RaycastBounds(touchPoint, _floorPaddleBounds)) {
                    floorHitPoint.x = Mathf.Clamp(floorHitPoint.x, _floorPaddleMinX, _floorPaddleMaxX); // Clamp paddle to min or max x of paddle's floor
                    floorHitPoint.y = Paddle.StartPoint.y;
                    Paddle.Move(floorHitPoint);
                }
                else { // Trim paddle to max z and clamp to min or max x of paddle's floor
                    Vector3 start = Paddle.StartPoint;
                    Vector3 end = floorHitPoint;
                    Debug.Assert(start.z <= end.z);
                    float t = (_floorPaddleMaxZ - start.z) / (end.z - start.z + 0.001f);
                    end.x = Mathf.Clamp(Mathf.Lerp(start.x, end.x, t), _floorPaddleMinX, _floorPaddleMaxX);
                    end.y = start.y;
                    end.z = _floorPaddleMaxZ;
                    Paddle.Move(end);
                }
                        
                return true;
            }

            bool TouchMoveBallAimer() {
                if (!BallAimer.Active) {
                    return false;
                }
                
                Debug.Assert((!Paddle.IsMoving || Paddle.Mode == PaddleMode.Sticky) && SelectedBall.Sleeping);
                if (!RaycastBounds(touchPoint, _floorPaddleBounds)) {
                    BallAimer.Aim(floorHitPoint);
                }

                return true;
            }

            void TouchMoveSelectedBall() {
                Debug.Assert(!BallAimer.Active && !Paddle.IsMoving);
                if (!SelectedBall.Sleeping && !RaycastBounds(touchPoint, _floorPaddleBounds)) {
                    SelectedBall.TouchMove(floorHitPoint);
                }
            }
        }

        public void TouchUp() {
            if (!_floorHasTouch) {
                return;
            }

            _floorHasTouch = false;
            _floorLongTapIsWait = false;

            Debug.Assert(HasSelectedBall);
            if (!HasSelectedBall) {
                BallAimer.Active = false;
                Paddle.Restart(PaddleMode.Default);
                return;
            }

            TouchUpSelectedBall();
            TouchUpPaddle();
            TouchUpBallAimer();

            void TouchUpSelectedBall() {
                SelectedBall.TouchUp();
                if (SelectedBall.Skilled) {
                    SelectedBall.Skilled = false;
                }
            }

            void TouchUpPaddle() {
                if (Paddle.IsMoving) {
                    Paddle.End();
                }

                if (Paddle.IsCompleted && Paddle.Mode == PaddleMode.Sticky && SelectedBall.Sleeping) {
                    SelectedBall.Sleeping = false;
                    Debug.Assert(BallAimer.Active);
                    BallAimer.Active = false;
                }
            }

            void TouchUpBallAimer() {
                if (!BallAimer.Active) {
                    return;
                }
                
                Debug.Assert(SelectedBall.Sleeping && !SelectedBall.Skilled);
                SelectedBall.Spawn(_playground.BallSpawnPosition, BallAimer.Direction);
                BallAimer.Active = false;
            }
        }

        private bool RaycastBounds(Vector3 screenPoint, Bounds bounds) => bounds.IntersectRay(_camera.ScreenPointToRay(screenPoint));

        private bool RaycastBounds(Vector3 screenPoint, Bounds bounds, out Vector3 hitPoint) {
            Ray ray = _camera.ScreenPointToRay(screenPoint);

            if (!bounds.IntersectRay(ray, out float distance)) {
                hitPoint = Vector3.zero;
                return false;
            }

            hitPoint = ray.GetPoint(distance);
            return true;
        }
    }
}
