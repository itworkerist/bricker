﻿using UnityEngine;

namespace Bricker.Level
{
    public class BombBrick : Brick
    {
        public override void PostConstruct(IBrickContext context) {
            base.PostConstruct(context);
            BallSpeedMultiplier = context.GetConfig<BombBrickConfig>(this).ballSpeedMultiplier;
        }

        public override BrickCategory Category => BrickCategory.Bomb;

        public float BallSpeedMultiplier { get; private set; }

        protected override void Collide(IBall ball) {
            base.Collide(ball);
            ball.SpeedBrickMultiplier = BallSpeedMultiplier;

            if (ball.Position.z <= Position.z) {
                ball.Direct(Vector3.back);
            }
        }
    }
}
