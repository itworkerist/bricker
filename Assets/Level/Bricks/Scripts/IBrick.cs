﻿using UnityEngine;

namespace Bricker.Level
{
    public enum BrickCategory
    {
        Simpleton,
        Bomb,
        Cannon
    }

    public interface IBrick
    {
        void PostConstruct(IBrickContext context);

        BrickCategory Category { get; }

        int CapsuleChance { get; }

        Vector3 Position { get; }
    }
}
