﻿using UnityEngine;

namespace Bricker.Level
{
    public class CannonBrick : Brick
    {
        [SerializeField] private Transform _unitTransform;
        [SerializeField] private Transform _propellerTransform;
        [SerializeField] private CapsuleCollider _collider;

        public override void PostConstruct(IBrickContext context) {
            base.PostConstruct(context);
            
            var config = context.GetConfig<CannonBrickConfig>(this);
            ImpactRadius = _collider.radius;
            ImpactMultiplier = config.impactMultiplier;
            ImpactTolerance = config.impactTolerance;

            _propellerTransform.Rotate(0, Random.Range(0, 90), 0);
        }

        public override BrickCategory Category => BrickCategory.Cannon;

        public float ImpactRadius { get; private set; }

        public float ImpactMultiplier { get; private set; }

        public float ImpactTolerance { get; private set; }

        protected void OnTriggerStay(Collider otherCollider) {
            if (Frozen) {
                return;
            }

            if (otherCollider.CompareTag(LevelConsts.kBallTag)) {
                IBall ball = otherCollider.GetComponent<IBall>();

                Debug.Assert(!ball.Sleeping);
                if (ball.Sleeping) {
                    return;
                }

                ball.SpeedPaddleSummand = 0;

                Vector3 impact = ball.Position - Position;
                impact.y = 0;
                impact.Normalize();
                Vector3 direction = ball.Velocity;
                direction.y = 0;

                if ((impact + direction.normalized).sqrMagnitude <= ImpactTolerance) {
                    return;
                }

                direction += impact * ImpactMultiplier;
                ball.Direct(direction);
            }
        }

        protected override void Update() {
            base.Update();

            IBall ball = Context.SelectedBall;
            if (ball != null && !Frozen) {
                Vector3 direction = ball.Position - Position;

                if (!ball.Sleeping) {
                    _unitTransform.localRotation = Quaternion.Euler(0, 0, Mathf.Atan2(direction.z, direction.x) * -Mathf.Rad2Deg - 90);
                }

                const float kMinSpeedPropellerRotation = 60;
                const float kMaxSpeedPropellerRotation = 540;
                float propellerDeltaAngle = ((direction.magnitude <= ImpactRadius && !ball.Sleeping) ? kMaxSpeedPropellerRotation : kMinSpeedPropellerRotation) * -Time.deltaTime;
                _propellerTransform.Rotate(0, propellerDeltaAngle, 0);
            }
        }
    }
}
