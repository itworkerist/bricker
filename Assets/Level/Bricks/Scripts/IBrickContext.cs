﻿namespace Bricker.Level
{
    public interface IBrickContext
    {
        IBall SelectedBall { get; }

        TBrickConfig GetConfig<TBrickConfig>(IBrick brick) where TBrickConfig : BrickConfig;

        void OnBrickDied(IBrick brick);
    }
}
