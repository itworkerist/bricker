﻿using UnityEngine;

namespace Bricker.Level
{
    [DisallowMultipleComponent]
    public abstract class Brick : MonoBehaviour, IBrick
    {
        [SerializeField] private GameObject _iceGameObject;
        [SerializeField] private Animation _damageAnimation;

        private float _frozenTime;

        public virtual void PostConstruct(IBrickContext context) {
            Context = context;

            var config = context.GetConfig<BrickConfig>(this);
            Health = config.health;
            FreezeDuration = config.freezeDuration;
            CapsuleChance = config.capsuleChance;

            _frozenTime = 0;
            Frozen = false;
        }

        public abstract BrickCategory Category { get; }

        public int Health { get; private set; }

        public float FreezeDuration { get; private set; }

        public int CapsuleChance { get; private set; }

        public bool Frozen {
            get => _frozenTime > 0;
            private set {
                _frozenTime = value ? FreezeDuration : 0;
                _iceGameObject.SetActive(Frozen);
            }
        }

        public Vector3 Position => transform.position;

        protected IBrickContext Context { get; private set; }

        protected void OnCollisionEnter(Collision collision) {
            GameObject go = collision.gameObject;
            if (go.CompareTag(LevelConsts.kBallTag)) {
                IBall ball = go.GetComponent<IBall>();

                if (ball.Category == BallCategory.Ice && ball.Skilled) {
                    Frozen = Health > 0;
                    return;
                }

                if (Frozen) {
                    CauseDamage(Health);
                    return;
                }

                Collide(ball);
            }
        }

        protected virtual void Collide(IBall ball) => CauseDamage(ball.Damage);

        protected virtual void Update() {
            if (!Frozen) {
                return;
            }

            _frozenTime -= Time.deltaTime;
            if (_frozenTime <= 0) {
                Frozen = false;
            }
        }

        private void CauseDamage(int damage) {
            Debug.Assert(damage > 0);
            if (damage <= 0) {
                return;
            }

            Debug.Assert(Health > 0);
            Health -= Mathf.Clamp(damage, 0, Health);
            
            if (Health <= 0) {
                Context.OnBrickDied(this);
                Destroy(gameObject);
            }
            else {
                _damageAnimation.Play();
            }
        }
    }
}
