﻿namespace Bricker.Level
{
    public class SimpletonBrick : Brick
    {
        public override BrickCategory Category => BrickCategory.Simpleton;
    }
}
