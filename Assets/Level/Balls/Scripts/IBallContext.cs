﻿namespace Bricker.Level
{
    public interface IBallContext
    {
        TBallConfig GetConfig<TBallConfig>(IBall ball) where TBallConfig : BallConfig;

        void OnBallSkillPowerChanged(float skillPower);

        void OnBallDied(IBall ball);
    }
}
