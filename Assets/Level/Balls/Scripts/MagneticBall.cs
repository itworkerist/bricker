﻿using UnityEngine;

namespace Bricker.Level
{
    public class MagneticBall : SkillfulBall
    {
        private bool _hasTarget;
        private Vector3 _target;
        private float _delay;

        public override void PostConstruct(IBallContext context) {
            var config = context.GetConfig<MagneticBallConfig>(this);
            SkillTimeInterval = config.skillTimeInterval;
            DistanceBallSpeedReduction = config.distanceBallSpeedReduction;
            base.PostConstruct(context);
        }

        public override void Restart() {
            base.Restart();
            _hasTarget = false;
            _target = Vector3.zero;
            _delay = SkillTimeInterval;
        }

        public override BallCategory Category => BallCategory.Magnetic;

        public float SkillTimeInterval { get; private set; }

        public float DistanceBallSpeedReduction { get; private set; }

        protected override void FixedUpdate() {
            base.FixedUpdate();

            if (_hasTarget && Skilled) {
                _delay -= Time.fixedDeltaTime;
                
                if (_delay < 0) {
                    _delay = SkillTimeInterval;
                    DirectToTarget();
                }
            }
        }

        public override void TouchDown(Vector3 touchPosition) => TouchMove(touchPosition);

        public override void TouchMove(Vector3 touchPosition) {
            _target = touchPosition;
            _hasTarget = true;
            _delay = 0;
        }

        public override void TouchUp() => _hasTarget = false;

        protected override void SkilledChanged() {
            base.SkilledChanged();

            SpeedSkillMultiplier = 1;
            if (Skilled) {
                _delay = SkillTimeInterval;
            }
        }

        private void DirectToTarget() {
            Debug.Assert(_hasTarget);
            if (!_hasTarget) {
                return;
            }

            Debug.Assert(!Sleeping);
            if (Sleeping) {
                return;
            }

            float distance = Vector3.Distance(Position, _target);
            if (distance < Radius) {
                SpeedSkillMultiplier = float.Epsilon;
                return;
            }

            SpeedSkillMultiplier = Mathf.Clamp(distance / DistanceBallSpeedReduction, float.Epsilon, 1);

            Vector3 direction = _target - Position;
            direction.y = 0;
            Direct(direction);
        }
    }
}
