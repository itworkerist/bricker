﻿using UnityEngine;

namespace Bricker.Level
{
    public enum BallCategory
    {
        Simple,
        Ice,
        Magnetic
    }

    public interface IBall
    {
        void PostConstruct(IBallContext context);

        BallCategory Category { get; }

        float SpeedCapsuleMultiplier { get; set; }

        float RadiusCapsuleMultiplier { get; set; }

        float DamageCapsuleMultiplier { get; set; }

        float SpeedBrickMultiplier { get; set; }

        float SpeedPaddleSummand { get; set; }

        float Radius { get; }

        int Damage { get; }

        bool Skilled { get; set; }

        bool Sleeping { get; set; }

        Vector3 Position { get; }

        Vector3 Velocity { get; }

        void Restart();

        void Locate(Vector3 position);

        void Direct(Vector3 direction);

        void Spawn(Vector3 position, Vector3 direction);

        void TouchDown(Vector3 touchPosition);

        void TouchMove(Vector3 touchPosition);

        void TouchUp();
    }
}
