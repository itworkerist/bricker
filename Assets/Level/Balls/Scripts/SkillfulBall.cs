﻿using UnityEngine;

namespace Bricker.Level
{
    public abstract class SkillfulBall : Ball
    {
        [SerializeField] private GameObject _skillGameObject;

        public override void PostConstruct(IBallContext context) {
            var config = context.GetConfig<SkillfulBallConfig>(this);
            SkillDuration = config.skillDuration;
            SkillCooldown = config.skillCooldown;
            base.PostConstruct(context);
        }

        public override void Restart() {
            base.Restart();
            _skillGameObject.SetActive(Skilled);
            SkillTime = SkillDuration;
        }

        public float SkillDuration { get; private set; }

        public float SkillCooldown { get; private set; }

        public float SkillTime {
            get => _skillTime;
            private set {
                _skillTime = (value <= SkillDuration) ? value : SkillDuration;
                Context.OnBallSkillPowerChanged((SkillTime > 0) ? SkillTime / SkillDuration : 0);
            }
        }
        private float _skillTime;

        protected override void Update() {
            base.Update();

            if (Skilled) {
                SkillTime -= Time.deltaTime;
            }
            else if (SkillTime < SkillDuration) {
                SkillTime = Mathf.Clamp(SkillTime + Time.deltaTime * SkillDuration / SkillCooldown, 0, SkillDuration);
            }
        }

        protected override void LateUpdate() {
            base.LateUpdate();
            
            if (_skillGameObject.activeSelf != Skilled) {
                SkilledChanged();
            }

            if (Skilled && SkillTime < 0) {
                Die();
            }
        }

        protected virtual void SkilledChanged() => _skillGameObject.SetActive(Skilled);
    }
}
