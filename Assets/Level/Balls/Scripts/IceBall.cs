﻿namespace Bricker.Level
{
    public class IceBall : SkillfulBall
    {
        public override BallCategory Category => BallCategory.Ice;
    }
}
