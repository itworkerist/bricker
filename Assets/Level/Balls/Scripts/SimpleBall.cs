﻿namespace Bricker.Level
{
    public class SimpleBall : Ball
    {
        public override BallCategory Category => BallCategory.Simple;
    }
}
