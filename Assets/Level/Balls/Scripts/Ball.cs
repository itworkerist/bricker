﻿using UnityEngine;

namespace Bricker.Level
{
    [DisallowMultipleComponent]
    public abstract class Ball : MonoBehaviour, IBall
    {
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private Transform _bodyTransform;
        [SerializeField] private TrailRenderer _trailRenderer;

        private Vector3 _direction;
        private bool _hasDirectionChanged;
        private bool _hasSpeedChanged;
        private bool _hasRadiusChanged;
        private bool _hasDamageChanged;
        private Vector3 _bodyRotation;

        public virtual void PostConstruct(IBallContext context) {
            Context = context;

            var config = Context.GetConfig<BallConfig>(this);
            SpeedDefault = config.speed;
            DamageDefault = config.damage;
            Vector3 scale = transform.localScale;
            RadiusDefault = Mathf.Max(scale.x, scale.y, scale.z) / 2f;
            
            Restart();
            gameObject.SetActive(false);
        }

        public virtual void Restart() {
            gameObject.SetActive(true);
            
            _direction = Vector3.forward;
            _hasDirectionChanged = false;

            _hasSpeedChanged = false;
            SpeedPaddleSummand = 0;
            SpeedCapsuleMultiplier = 1;
            SpeedBrickMultiplier = 1;
            SpeedSkillMultiplier = 1;
            RecalculateSpeed();

            _hasRadiusChanged = false;
            RadiusCapsuleMultiplier = 1;

            _hasDamageChanged = false;
            DamageCapsuleMultiplier = 1;
            DamageSkillMultiplier = 1;
            RecalculateDamage();

            Skilled = false;
            Sleeping = true;

            _bodyRotation = Vector3.zero;
            _trailRenderer.Clear();

            Context.OnBallSkillPowerChanged(1);
        }

        protected virtual void FixedUpdate() {
            UpdateRadius();
            UpdateVelocity();
        }

        public virtual void OnCollisionExit(Collision collision) {
            GameObject go = collision.gameObject;
            if (go.CompareTag(LevelConsts.kWallTag)) {
                SpeedPaddleSummand = 0;

                if (go.name == LevelConsts.kWallBottomName) {
                    Die();
                }
                else {
                    const float kRandomDeviationByCollisionWall = 1.5f;
                    Vector3 direction = Velocity;
                    direction.z += (Mathf.Sign(direction.z) < 0) ? Random.Range(-kRandomDeviationByCollisionWall, 0) : Random.Range(0, kRandomDeviationByCollisionWall);
                    Direct(direction);
                }
            }
            else if (go.CompareTag(LevelConsts.kBrickTag)) {
                SpeedPaddleSummand = 0;
            }
        }

        protected virtual void Update() {
            UpdateBody();
            Debug.DrawLine(Position, Position + Velocity * 0.125f, Color.green, 0.25f);
        }

        protected virtual void LateUpdate() {
            RecalculateDamage();

            // Check equality of speed and rigidbody's velocity
            const float kSpeedTolerance = 1f;
            if (!_hasSpeedChanged && !Sleeping && Mathf.Abs(Velocity.sqrMagnitude - Speed * Speed) > kSpeedTolerance) {
                _hasSpeedChanged = true;
                //Debug.LogWarningFormat("Ball: Speed = {0} not equal rigidbody velocity = {1}", Speed, Velocity.magnitude);
            }
        }

        protected void Die() {
            Debug.Assert(!Sleeping);
            Context.OnBallDied(this);
            Destroy(gameObject);
        }

        private void UpdateBody() {
            if (Sleeping) {
                return;
            }

            const float kRotationSpeedMultiplier = 45f;
            _bodyRotation.z -= Speed * Time.deltaTime * kRotationSpeedMultiplier;
            _bodyRotation.y = Mathf.Atan2(Velocity.z, Velocity.x) * -Mathf.Rad2Deg;
            _bodyTransform.eulerAngles = _bodyRotation;
        }

        public abstract BallCategory Category { get; }

        public bool Skilled {
            get => _skilled;
            set {
                _skilled = value;
                Debug.Assert(!Sleeping || !Skilled);
            }
        }
        private bool _skilled;

        public bool Sleeping {
            get => _rigidbody.isKinematic;
            set {
                if (value) {
                    _rigidbody.Sleep();
                    _rigidbody.isKinematic = true;
                    _rigidbody.detectCollisions = false;
                    SpeedPaddleSummand = 0;
                }
                else {
                    _rigidbody.detectCollisions = true;
                    _rigidbody.isKinematic = false;
                    _rigidbody.WakeUp();
                    _hasSpeedChanged = true; // Update rigidbody's velocity after wake up
                }

                _trailRenderer.enabled = !Sleeping;
                Debug.Assert(!Sleeping || !Skilled);
            }
        }

        public Vector3 Position {
            get => _rigidbody.position;
            private set {
                Debug.Assert(Sleeping);
                _rigidbody.position = value;
            }
        }

        public Vector3 Velocity {
            get => _rigidbody.velocity;
            private set {
                Debug.Assert(!_hasSpeedChanged);
                value.y = 0;
                _rigidbody.velocity = value;
                Debug.Assert(Velocity.sqrMagnitude > 0);
            }
        }

        protected IBallContext Context { get; private set; }

        private void UpdateVelocity() {
            if (!_hasSpeedChanged && !_hasDirectionChanged) {
                return;
            }

            if (Sleeping) {
                return;
            }

            RecalculateSpeed();
            if (_hasDirectionChanged) {
                Velocity = Speed * _direction.normalized;
                _hasDirectionChanged = false;
            }
            else {
                Velocity = Speed * Velocity.normalized;
            }

            const float kTrailTimeMultiplier = 0.2f;
            _trailRenderer.time = Mathf.Max(Mathf.Log10(Velocity.magnitude * Radius), 1) * kTrailTimeMultiplier;
        }

        public void Locate(Vector3 position) {
            position.y = Position.y;
            Position = position;
            _trailRenderer.Clear();
        }

        public void Direct(Vector3 direction) {
            Debug.Assert(direction.sqrMagnitude > 0);
            if (direction.sqrMagnitude <= 0) {
                return;
            }

            _direction = direction;
            _hasDirectionChanged = true;
            Debug.DrawRay(Position, _direction, Color.blue, 2);
        }

        public void Spawn(Vector3 position, Vector3 direction) {
            Restart();
            Locate(position);
            Direct(direction);
            Sleeping = false;
        }

        public virtual void TouchDown(Vector3 touchLocation) {}

        public virtual void TouchMove(Vector3 touchLocation) {}

        public virtual void TouchUp() {}

        public float Speed {
            get => _speed;
            private set {
                _speed = value;
                Debug.Assert(_hasSpeedChanged);
            }
        }
        private float _speed;

        public float SpeedDefault {
            get => _speedDefault;
            private set {
                _speedDefault = value;
                _hasSpeedChanged = true;
            }
        }
        private float _speedDefault;

        public float SpeedPaddleSummand {
            get => _speedPaddleSummand;
            set {
                _speedPaddleSummand = value;
                _hasSpeedChanged = true;
            }
        }
        private float _speedPaddleSummand;

        public float SpeedCapsuleMultiplier {
            get => _speedCapsuleMultiplier;
            set {
                _speedCapsuleMultiplier = value;
                _hasSpeedChanged = true;
            }
        }
        private float _speedCapsuleMultiplier;

        public float SpeedBrickMultiplier {
            get => _speedBrickMultiplier;
            set {
                _speedBrickMultiplier = value;
                _hasSpeedChanged = true;
            }
        }
        private float _speedBrickMultiplier;

        public float SpeedSkillMultiplier {
            get => _speedSkillMultiplier;
            protected set {
                _speedSkillMultiplier = value;
                _hasSpeedChanged = true;
            }
        }
        private float _speedSkillMultiplier;

        private void RecalculateSpeed() {
            if (!_hasSpeedChanged) {
                return;
            }

            if (!Mathf.Approximately(SpeedBrickMultiplier, 1)) {
                SpeedPaddleSummand = 0;
                SpeedCapsuleMultiplier = 1;
            }
            else if (!Mathf.Approximately(SpeedCapsuleMultiplier, 1) || !Mathf.Approximately(SpeedSkillMultiplier, 1)) {
                SpeedPaddleSummand = 0;
            }

            Speed = (SpeedDefault + SpeedPaddleSummand) * SpeedCapsuleMultiplier * SpeedBrickMultiplier * SpeedSkillMultiplier;
            _hasSpeedChanged = false;
        }

        public float Radius {
            get => _radius;
            private set {
                _radius = value;
                Debug.Assert(_hasRadiusChanged);
            }
        }
        private float _radius;

        public float RadiusDefault {
            get => _radiusDefault;
            private set {
                _radiusDefault = value;
                _hasRadiusChanged = true;
            }
        }
        private float _radiusDefault;

        public float RadiusCapsuleMultiplier {
            get => _radiusCapsuleMultiplier;
            set {
                _radiusCapsuleMultiplier = value;
                _hasRadiusChanged = true;
            }
        }
        private float _radiusCapsuleMultiplier;

        private void UpdateRadius() {
            if (!_hasRadiusChanged) {
                return;
            }

            Radius = RadiusDefault * RadiusCapsuleMultiplier;

            float diameter = Radius * 2;
            _rigidbody.transform.localScale = Vector3.one * diameter;
            _trailRenderer.startWidth = diameter * 2;
            _trailRenderer.endWidth = diameter;

            _hasRadiusChanged = false;
        }

        public int Damage {
            get => _damage;
            private set {
                _damage = value;
                Debug.Assert(_hasDamageChanged);
            }
        }
        private int _damage;

        public int DamageDefault { get; private set; }

        public float DamageCapsuleMultiplier {
            get => _damageCapsuleMultiplier;
            set {
                _damageCapsuleMultiplier = value;
                _hasDamageChanged = true;
            }
        }
        private float _damageCapsuleMultiplier;

        public float DamageSkillMultiplier {
            get => _damageSkillMultiplier;
            set {
                _damageSkillMultiplier = value;
                _hasDamageChanged = true;
            }
        }
        private float _damageSkillMultiplier;

        private void RecalculateDamage() {
            if (!_hasDamageChanged) {
                return;
            }

            Damage = (int)(DamageDefault * DamageCapsuleMultiplier * DamageSkillMultiplier);
            _hasDamageChanged = false;
        }
    }
}
