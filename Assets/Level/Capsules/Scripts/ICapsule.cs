﻿using UnityEngine;

namespace Bricker.Level
{
    public enum CapsuleCategory
    {
        StickyPaddle,
        FallingPaddle,
        BigBall,
        SmallBall,
        SlowBall,
        QuickBall
    }

    public interface ICapsule
    {
        void PostConstruct(ICapsuleHandler handler);

        CapsuleCategory Category { get; }

        void Spawn(Vector3 position, float speed);

        void Despawn();
    }
}
