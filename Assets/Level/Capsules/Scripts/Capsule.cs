﻿using UnityEngine;

namespace Bricker.Level
{
    public class Capsule : MonoBehaviour, ICapsule
    {
        [SerializeField] private CapsuleCategory _category;
        
        private ICapsuleHandler _handler;

        public void PostConstruct(ICapsuleHandler handler) => _handler = handler;

        public float Speed { get; private set; }

        protected void OnTriggerEnter(Collider otherCollider) {
            if (otherCollider.CompareTag(LevelConsts.kPaddleTag)) {
                _handler.OnCapsuleFinished(this, true);
            }
            else if (otherCollider.CompareTag(LevelConsts.kWallTag)) {
                _handler.OnCapsuleFinished(this, false);
            }
        }

        protected void Update() => transform.Translate(0, 0, -Speed * Time.deltaTime);

        public CapsuleCategory Category => _category;

        public void Spawn(Vector3 position, float speed) {
            position.y = transform.position.y;
            transform.position = position;
            Speed = speed;
            gameObject.SetActive(true);
        }

        public void Despawn() => gameObject.SetActive(false);
    }
}
