﻿namespace Bricker.Level
{
    public interface ICapsuleHandler
    {
        void OnCapsuleFinished(ICapsule capsule, bool wasCaught);
    }
}
