﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Bricker.Level
{
    [DisallowMultipleComponent]
    public class LevelUI : MonoBehaviour
    {
        [SerializeField] private GameObject _menuGameObject;
        [SerializeField] private Text _scoreText;
        [SerializeField] private GameObject _inventoryGameObject;
        [SerializeField] private Image _inventoryFirstBallImage;
        [SerializeField] private Image _inventorySecondBallImage;
        [SerializeField] private Image _inventoryThirdBallImage;
        [SerializeField] private Image _inventoryBallSkillPowerImage;

        [SerializeField] private GameObject _winLoseGameObject;
        [SerializeField] private Image _winLoseBackImage;
        [SerializeField] private GameObject _popupTitleWinGameObject;
        [SerializeField] private GameObject _popupTitleLoseGameObject;
        [SerializeField] private Image _popupLeftStarImage;
        [SerializeField] private Image _popupCenterStarImage;
        [SerializeField] private Image _popupRightStarImage;
        [SerializeField] private Text _popupScoreText;
        [SerializeField] private Image _popupLeftBallImage;
        [SerializeField] private Image _popupCenterBallImage;
        [SerializeField] private Image _popupRightBallImage;
        [SerializeField] private Button _popupReplayButton;

        [SerializeField] private Sprite _iconSimpleBall;
        [SerializeField] private Sprite _iconIceBall;
        [SerializeField] private Sprite _iconMagneticBall;
        [SerializeField] private Color _winBackColor;
        [SerializeField] private Color _loseBackColor;

        private int _score;
        private List<BallCategory> _inventoryBalls;
        private IDictionary<BallCategory, Sprite> _ballIcons;
        
        protected void Awake() {
            _menuGameObject.SetActive(true);
            _winLoseGameObject.SetActive(false);

            _score = 0;
            _inventoryBalls = new List<BallCategory>(3);
            _ballIcons = new Dictionary<BallCategory, Sprite> {
                { BallCategory.Ice, _iconIceBall },
                { BallCategory.Magnetic, _iconMagneticBall },
                { BallCategory.Simple, _iconSimpleBall }
            };
        }

        public void SetScore(int score) {
            _score = score;
            _scoreText.text = _score.ToString();
        }

        public void SetInventoryBalls(IEnumerable<IBall> balls) {
            _inventoryBalls.Clear();
            _inventoryBalls.AddRange(balls.Select(b => b.Category));

            if (_inventoryBalls.Count <= 0) {
                _inventoryGameObject.SetActive(false);
                return;
            }

            _inventoryGameObject.SetActive(true);
            _inventoryFirstBallImage.enabled = _inventoryBalls.Count >= 1;
            _inventorySecondBallImage.enabled = _inventoryBalls.Count >= 2;
            _inventoryThirdBallImage.enabled = _inventoryBalls.Count >= 3;

            if (!_inventoryFirstBallImage.enabled) {
                return;
            }
            
            _inventoryFirstBallImage.sprite = GetIconBall(0);

            if (!_inventorySecondBallImage.enabled) {
                return;
            }
            
            _inventorySecondBallImage.sprite = GetIconBall(1);
            if (_inventoryThirdBallImage.enabled) {
                _inventoryThirdBallImage.sprite = GetIconBall(2);
            }
        }

        public void SetBallSkillPower(float skillPower) {
            skillPower = Mathf.Clamp01(skillPower);
            _inventoryBallSkillPowerImage.fillAmount = skillPower;
            _inventoryBallSkillPowerImage.color = Color.Lerp(Color.red, Color.green, skillPower);
        }

        public void ShowWinLosePopup(float levelProgress) {
            levelProgress = Mathf.Clamp01(levelProgress);
            bool win = levelProgress > 0;

            _menuGameObject.SetActive(false);
            _winLoseGameObject.SetActive(true);
            _winLoseBackImage.color = win ? _winBackColor : _loseBackColor;
            _popupTitleWinGameObject.SetActive(win);
            _popupTitleLoseGameObject.SetActive(!win);

            StartCoroutine(FillStars(levelProgress));
            StartCoroutine(LerpScore());

            _popupLeftBallImage.enabled = _inventoryBalls.Count >= 1;
            _popupCenterBallImage.enabled = _inventoryBalls.Count >= 2;
            _popupRightBallImage.enabled = _inventoryBalls.Count >= 3;
            if (_popupLeftBallImage.enabled) {
                _popupLeftBallImage.sprite = GetIconBall(0);

                if (_popupCenterBallImage.enabled) {
                    _popupCenterBallImage.sprite = GetIconBall(1);

                    if (_popupRightBallImage.enabled) {
                        _popupRightBallImage.sprite = GetIconBall(2);
                    }
                }
            }

            _popupReplayButton.interactable = true;
            _popupReplayButton.onClick.RemoveAllListeners();
            _popupReplayButton.onClick.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex));
        }

        private Sprite GetIconBall(int index) => _ballIcons[_inventoryBalls[index]];

        private IEnumerator FillStars(float levelProgress) {
            const float kFillStarsDuration = 1.25f;
            for (float progress = 0, time = 0; progress <= levelProgress; time += Time.unscaledDeltaTime, progress = Mathf.SmoothStep(0, levelProgress, time / kFillStarsDuration)) {
                _popupLeftStarImage.fillAmount = Mathf.InverseLerp(0, 0.33f, progress);
                _popupCenterStarImage.fillAmount = Mathf.InverseLerp(0.34f, 0.66f, progress);
                _popupRightStarImage.fillAmount = Mathf.InverseLerp(0.67f, 1, progress);
                yield return null;
            }
        }

        private IEnumerator LerpScore() {
            for (float score = 0; score < _score; score += Time.unscaledDeltaTime * _score) {
                _popupScoreText.text = ((int)score).ToString();
                yield return null;
            }

            _popupScoreText.text = _score.ToString();
        }
    }
}
